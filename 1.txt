pipeline {
  agent {
    node {
      label 'maven'
    }

  }
  stages {
    stage('拉取代码') {
      agent none
      steps {
        container('maven') {
          git(url: 'https://gitee.com/L1692312138/RuoYi-Cloud.git', credentialsId: 'gitee-lsh-monitor', branch: 'master', changelog: true, poll: false)
          sh 'ls -al'
        }

      }
    }

    stage('项目编译') {
      agent none
      steps {
        container('maven') {
          sh 'mvn clean package  -Dmaven.test.skip=true'
          sh 'ls -al'
        }

      }
    }

    stage('default-2') {
      parallel {
        stage('构建monitor镜像') {
          agent none
          steps {
            container('maven') {
              sh 'ls -l ruoyi-visual/ruoyi-monitor/target/'
              sh 'docker build -t ruoyi-monitor:latest -f ruoyi-visual/ruoyi-monitor/Dockerfile ./ruoyi-visual/ruoyi-monitor/'
            }

          }
        }

        stage('构建gateway镜像') {
          agent none
          steps {
            container('maven') {
              sh 'ls'
            }

          }
        }

      }
    }

    stage('default-3') {
      parallel {
        stage('推送monitor镜像') {
          agent none
          steps {
            container('maven') {
              withCredentials([usernamePassword(credentialsId : 'aliyun-repository' ,passwordVariable : 'DOCKER_PWD_VAR' ,usernameVariable : 'DOCKER_USER_VAR' ,)]) {
                sh 'echo "$DOCKER_PWD_VAR" | docker login $REGISTRY -u "$DOCKER_USER_VAR" --password-stdin'
                sh 'docker tag   ruoyi-monitor:latest $REGISTRY/$DOCKERHUB_NAMESPACE/ruoyi-monitor:SNAPSHOT-$BUILD_NUMBER'
                sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/ruoyi-monitor:SNAPSHOT-$BUILD_NUMBER'
              }

            }

          }
        }

        stage('推送xxx镜像') {
          agent none
          steps {
            container('maven') {
              sh 'ls'
            }

          }
        }

      }
    }

    stage('default-4') {
      parallel {
        stage('monitor部署k8s') {
          agent none
          steps {
            kubernetesDeploy(enableConfigSubstitution: true, deleteResource: false, kubeconfigId: 'demo-kubeconfig', configs: 'ruoyi-visual/ruoyi-monitor/deploy/**')
          }
        }

        stage('xxx部署k8s') {
          agent none
          steps {
            container('maven') {
              sh 'ls'
            }

          }
        }

      }
    }

    stage('邮件确认') {
      agent none
      steps {
        mail(to: 'liushihao1210@qq.com', subject: 'RuoyiCloud流水线执行结果', body: 'RuoyiCloud Monitor服务 DevOps流水线 第 "$BUILD_NUMBER" 次 执行成功！ ', cc: '827875054@qq.com')
      }
    }

  }
  environment {
    DOCKER_CREDENTIAL_ID = 'dockerhub-id'
    GITHUB_CREDENTIAL_ID = 'github-id'
    KUBECONFIG_CREDENTIAL_ID = 'demo-kubeconfig'
    REGISTRY = 'registry.cn-shanghai.aliyuncs.com'
    DOCKERHUB_NAMESPACE = 'lsh_k8s_repository'
    ALIYUNHUB_NAMESPACE = 'lsh_k8s_repository'
    GITHUB_ACCOUNT = 'kubesphere'
    APP_NAME = 'devops-java-sample'
  }
  parameters {
    string(name: 'TAG_NAME', defaultValue: '', description: '')
  }
}